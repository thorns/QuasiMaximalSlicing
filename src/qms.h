#ifndef QMS_QMS_H
#define QMS_QMS_H

#include "common.h"

#if HAVE_OPENCL
#include <cl.h>
#endif

#include <inttypes.h>

#include "cctk.h"

#include "qms_solve.h"

typedef struct QMSContext QMSContext;

int qms_maximal_solve(QMSContext *qms);

#endif /* QMS_QMS_H */

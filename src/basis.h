/*
 * Basis sets for pseudospectral methods
 * Copyright (C) 2016 Anton Khirnov <anton@khirnov.net>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef QMS_BASIS_H
#define QMS_BASIS_H

/* a set of basis functions */
typedef struct BasisSet {
    /* evaluate the idx-th basis function at the specified point*/
    double (*eval)      (double coord, int idx);
    /* evaluate the first derivative of the idx-th basis function at the specified point*/
    double (*eval_diff1)(double coord, int idx);
    /* evaluate the second derivative of the idx-th basis function at the specified point*/
    double (*eval_diff2)(double coord, int idx);
    /**
     * Get the idx-th collocation point for the specified order.
     * idx runs from 0 to order - 1 (inclusive)
     */
    double (*colloc_point)(int order, int idx);
} BasisSet;

extern const BasisSet qms_cheb_basis;
extern const BasisSet qms_cheb_even_basis;
extern const BasisSet qms_full_basis;
extern const BasisSet qms_tb_even_basis;
extern const BasisSet qms_sb_even_basis;
extern const BasisSet qms_sb_odd_basis;
extern const BasisSet qms_tl_basis;
extern const BasisSet qms_cos_even_basis;

#define SCALE_FACTOR scale_factor
extern double scale_factor;

#endif /* QMS_BASIS_H */

/*
 * Quasimaximal slicing -- actual solver code
 * Copyright (C) 2016 Anton Khirnov <anton@khirnov.net>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef QMS_SOLVE_H
#define QMS_SOLVE_H

#include "common.h"

#include "cctk.h"

#include "basis.h"

typedef struct QMSSolverPriv QMSSolverPriv;

typedef struct QMSSolver {
    QMSSolverPriv *priv;

    const BasisSet *basis[2];

    int nb_coeffs[2];
    int nb_colloc_points[2];

    double *coeffs;
} QMSSolver;

int qms_solver_init(QMSSolver **ctx,
                    cGH *cctkGH,
                    int basis_order_r, int basis_order_z,
                    double outer_bound, double filter_power, double input_filter_power,
                    int ccz4);

void qms_solver_free(QMSSolver **ctx);

int qms_solver_solve(QMSSolver *ctx);

void qms_solver_print_stats(QMSSolver *ctx);

#endif /* QMS_SOLVE_H */
